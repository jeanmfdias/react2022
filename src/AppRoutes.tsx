import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Profile } from "./pages/Profile";
import { Tweets } from "./pages/Tweets";

export function AppRoutes() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Tweets/>}/>
                <Route path="/me" element={<Profile/>}/>
            </Routes>
        </BrowserRouter>
    )
}