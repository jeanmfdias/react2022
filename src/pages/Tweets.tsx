import { useState } from "react"
import { Link } from "react-router-dom"
import { Tweet } from "../components/Tweet"

export function Tweets() {
    const [tweets, setTweet] = useState<string[]>([
        "Tweet 1",
        "Tweet 2",
        "Tweet 3",
        "Tweet 4"
    ])

    function sendNewTweet() {
        setTweet([...tweets, 'New Tweet'])
    }

    return (
        <div>
            <h1>Tweets</h1>
            <div>
                {tweets.map(tweet => {
                    return <Tweet text={tweet} />
                })}

                <button onClick={sendNewTweet}>send tweet</button>
                
                <p>
                    <Link to="/me">Me</Link>
                </p>
            </div>
        </div>
    )
}