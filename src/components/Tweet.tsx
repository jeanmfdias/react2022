type TweetProp = {
    text: string
}

export function Tweet(props: TweetProp) {
    return <p>{props.text}</p>
}